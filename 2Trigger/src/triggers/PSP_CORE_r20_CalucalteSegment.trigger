trigger PSP_CORE_r20_CalucalteSegment on APS_Survey_Response__c (after insert) {
  
    public String moriskkyScore;
    public String morisky8_Score;
    public String BMQ_Necssity;
    public String BMQ_Concerns;
    public String BIPQ;
    boolean checkMorisky=true;
    Map<Integer,String> monthMap=new Map<Integer,String>();
/*** Commit to be ***/
    
  List<PSP_CORE_r20_Segementation__c> segmentList=new List<PSP_CORE_r20_Segementation__c>();
  List<PSP_CORE_r20_Segementation__c> updateSegmentList=new List<PSP_CORE_r20_Segementation__c>();
  List<APS_Survey_Response__c> previousResponses=[SELECT PSP_CORE_r20_Assessment__c,PSP_CORE_r20_Score_1__c,PSP_CORE_r20_Score_2__c,PSP_CORE_r20_Score_3__c,PSP_CORE_r20_Score_4__c,PSP_CORE_r20_Score_5__c,PSP_CORE_r20_Score_6__c,PSP_CORE_r20_Score_7__c,PSP_CORE_r20_Score_8__c,PSP_CORE_r20_Score_9__c,PSP_CORE_r20_BIPQ__c,PSP_CORE_r20_BMQ_Necessity__c,PSP_CORE_r20_BMQ_Concerns__c,APS_Number_of_SurveyResults__c,PSP_CORE_r20_Case__c ,PSP_CORE_r20_Program_Affiliation__c, PSP_CORE_r20_Survey_Administration__c, APS_SurveyName__c, Name, PSP_CORE_r20_Total_Score__c,PSP_CORE_r20_Survey_Administration__r.Name,PSP_CORE_r20_Submitted_Date__c FROM APS_Survey_Response__c where PSP_CORE_r20_Program_Affiliation__c=:Trigger.New[0].PSP_CORE_r20_Program_Affiliation__c];
  List<APS_Survey_Response__c> responseToBeUpdated=new List<APS_Survey_Response__c>();
   system.debug('test@@@'+previousResponses);
  if(!previousResponses.isEmpty()){
    for(APS_Survey_Response__c sResponse:previousResponses){
        if(previousResponses.size()==2 && previousResponses[1].PSP_CORE_r20_Survey_Administration__r.Name=='Patient Profile'){
            checkMorisky=false;
        }
        if(sResponse.PSP_CORE_r20_Survey_Administration__r.Name=='Morisky-4'){
          if(sResponse.PSP_CORE_r20_Score_1__c>2){
            moriskkyScore='Non-Adherent';
          }else {
            moriskkyScore='Adherent';
          }
        }
        //Added by Naveen, for MSCare Assessments    Date: 4/12/2016  -- Start
        if(sResponse.PSP_CORE_r20_Survey_Administration__r.Name=='Morisky-8' && checkMorisky){
          if(sResponse.PSP_CORE_r20_Score_1__c>2){
            morisky8_Score='Non-Adherent';
          }else {
            morisky8_Score='Adherent';
          }
        }
        //Added by Naveen, for MSCare Assessments    Date: 4/12/2016  -- End
        if(sResponse.PSP_CORE_r20_Survey_Administration__r.Name=='BMQ'){
              if(sResponse.PSP_CORE_r20_Score_1__c>=18){
                        BMQ_Necssity='Equal or above 18';
            }else {
                        BMQ_Necssity='Below 18';
            }
            if(sResponse.PSP_CORE_r20_Score_2__c>=13){
                        BMQ_Concerns='Equal or above 13';
            } else {
                        BMQ_Concerns='Below 13';
            }
        }
        
        if(sResponse.PSP_CORE_r20_Survey_Administration__r.Name=='B-IPQ'){
          if(sResponse.PSP_CORE_r20_Score_1__c>=38){
                    BIPQ='Equal or above 38';
            } else {
                    BIPQ='Below 38';
            }
        }
        
        if(sResponse.PSP_CORE_r20_Survey_Administration__r.Name=='Patient Profile'){
          List<PSP_CORE_r20_Segementation__c> patientSegmentation=[select id,PSP_CORE_r20_Score_1__c,PSP_CORE_r20_Score_2__c,PSP_CORE_r20_Score_3__c,PSP_CORE_r20_Score_4__c,PSP_CORE_r20_Score_5__c,PSP_CORE_r20_Score_6__c,PSP_CORE_r20_Score_7__c,PSP_CORE_r20_Score_8__c,PSP_CORE_r20_Score_9__c from PSP_CORE_r20_Segementation__c where PSP_CORE_r20_Case__c=:sResponse.PSP_CORE_r20_Case__c and PSP_CORE_r20_Morsiky__c!=null and (PSP_CORE_r20_Segment_allocation__c='Intensive' OR PSP_CORE_r20_Segment_allocation__c='Standard') order by lastModifiedDate DESC limit 1];
          List<APS_Survey_Response__c> pResponses=[select id from APS_Survey_Response__c where PSP_CORE_r20_Survey_Administration__r.Name='Patient Profile' and PSP_CORE_r20_Case__c=:sResponse.PSP_CORE_r20_Case__c];
          if(patientSegmentation!=null && patientSegmentation.size()==1 && pResponses.size()==1){
              patientSegmentation[0].PSP_CORE_r20_Score_1__c=sResponse.PSP_CORE_r20_Score_1__c;
              patientSegmentation[0].PSP_CORE_r20_Score_2__c=sResponse.PSP_CORE_r20_Score_2__c;
              patientSegmentation[0].PSP_CORE_r20_Score_3__c=sResponse.PSP_CORE_r20_Score_3__c;
              patientSegmentation[0].PSP_CORE_r20_Score_4__c=sResponse.PSP_CORE_r20_Score_4__c;
              patientSegmentation[0].PSP_CORE_r20_Score_5__c=sResponse.PSP_CORE_r20_Score_5__c;
              patientSegmentation[0].PSP_CORE_r20_Score_6__c=sResponse.PSP_CORE_r20_Score_6__c;
              patientSegmentation[0].PSP_CORE_r20_Score_7__c=sResponse.PSP_CORE_r20_Score_7__c;
              patientSegmentation[0].PSP_CORE_r20_Score_8__c=sResponse.PSP_CORE_r20_Score_8__c;
              patientSegmentation[0].PSP_CORE_r20_Score_9__c=sResponse.PSP_CORE_r20_Score_9__c;
              sResponse.PSP_CORE_r20_Segmentation__c=patientSegmentation[0].id;
              responseToBeUpdated.add(sResponse);
              updateSegmentList.add(patientSegmentation[0]);

          }else {
              PSP_CORE_r20_Segementation__c userSegmentation=new PSP_CORE_r20_Segementation__c(PSP_CORE_r20_Case__c=sResponse.PSP_CORE_r20_Case__c,PSP_CORE_r20_Score_1__c=sResponse.PSP_CORE_r20_Score_1__c,PSP_CORE_r20_Score_2__c=sResponse.PSP_CORE_r20_Score_2__c,PSP_CORE_r20_Score_3__c=sResponse.PSP_CORE_r20_Score_3__c,PSP_CORE_r20_Score_4__c=sResponse.PSP_CORE_r20_Score_4__c,PSP_CORE_r20_Score_5__c=sResponse.PSP_CORE_r20_Score_5__c,PSP_CORE_r20_Score_6__c=sResponse.PSP_CORE_r20_Score_6__c,PSP_CORE_r20_Score_7__c=sResponse.PSP_CORE_r20_Score_7__c,PSP_CORE_r20_Score_8__c=sResponse.PSP_CORE_r20_Score_8__c,PSP_CORE_r20_Score_9__c=sResponse.PSP_CORE_r20_Score_9__c);
              segmentList.add(userSegmentation);
          }
          /*
          PSP_CORE_r20_Segementation__c userSegmentation=new PSP_CORE_r20_Segementation__c(PSP_CORE_r20_Case__c=sResponse.PSP_CORE_r20_Case__c,PSP_CORE_r20_Score_1__c=sResponse.PSP_CORE_r20_Score_1__c,PSP_CORE_r20_Score_2__c=sResponse.PSP_CORE_r20_Score_2__c,PSP_CORE_r20_Score_3__c=sResponse.PSP_CORE_r20_Score_3__c,PSP_CORE_r20_Score_4__c=sResponse.PSP_CORE_r20_Score_4__c,PSP_CORE_r20_Score_5__c=sResponse.PSP_CORE_r20_Score_5__c,PSP_CORE_r20_Score_6__c=sResponse.PSP_CORE_r20_Score_6__c,PSP_CORE_r20_Score_7__c=sResponse.PSP_CORE_r20_Score_7__c,PSP_CORE_r20_Score_8__c=sResponse.PSP_CORE_r20_Score_8__c,PSP_CORE_r20_Score_9__c=sResponse.PSP_CORE_r20_Score_9__c);
          segmentList.add(userSegmentation);
          */
        }
        
        if(moriskkyScore!=null && moriskkyScore!='' && BMQ_Necssity!=null && BMQ_Necssity!='' && BMQ_Concerns!=null && BMQ_Concerns!='' && BIPQ!=null && BIPQ!=''){
            monthMap.put(01,'Jan');
            monthMap.put(02,'Feb');
            monthMap.put(03,'Mar');
            monthMap.put(04,'April');
            monthMap.put(05,'May');
            monthMap.put(06,'June');
            monthMap.put(07,'July');
            monthMap.put(08,'Aug');
            monthMap.put(09,'Sep');
            monthMap.put(10,'Oct');
            monthMap.put(11,'Nov');
            monthMap.put(13,'Dec');
           Integer dayValue=Date.today().day();
           Integer monthValue=Date.today().month();
           Integer yearValue=Date.today().year();
           String finalDate=monthMap.get(monthValue)+' '+String.valueOf(dayValue)+' '+String.valueOf(yearValue);
          system.debug(moriskkyScore+'moriskkyScore');
          system.debug(BIPQ+'BIPQ');
          system.debug(BMQ_Necssity+'BMQ_Necssity');
          system.debug(BMQ_Concerns+'BMQ_Concerns');
          PSP_CORE_r20_Segementation__c segmentation=[select PSP_CORE_r20_Segment_allocation__c from PSP_CORE_r20_Segementation__c where PSP_CORE_r20_Morsiky__c=:moriskkyScore AND PSP_CORE_r20_BMQ_Necessity__c=:BMQ_Necssity AND PSP_CORE_r20_BMQ_Concerns__c=:BMQ_Concerns AND PSP_CORE_r20_BIPQ__c=:BIPQ AND PSP_CORE_r20_Master_Data__c=true];
          sResponse.PSP_CORE_r20_Cluster__c=segmentation.PSP_CORE_r20_Segment_allocation__c;
          Case mCase=[select id,PSP_CORE_Adherence_Outcome__c,PSP_CORE_r20_Completion_of_Assessments__c,PSP_CORE_r20_Default_Segmentation__c from Case where id=:sResponse.PSP_CORE_r20_Case__c];
          mCase.PSP_CORE_Adherence_Outcome__c=segmentation.PSP_CORE_r20_Segment_allocation__c;
          mCase.PSP_CORE_r20_Completion_of_Assessments__c=Date.today();
          mCase.PSP_CORE_r20_Default_Segmentation__c=false;
          mCase.PSP_CORE_r20_Skip_Validation__c = true;
          APS_Util_Constants.SEGREC1=false; //Added by Naveen DT-1043
          update mCase;
         
          PSP_CORE_r20_Segementation__c userSegmentation=new PSP_CORE_r20_Segementation__c(PSP_CORE_r20_Case__c=mCase.id,PSP_CORE_r20_Survey_Date__c=finalDate,PSP_CORE_r20_Case_Segment__c=segmentation.PSP_CORE_r20_Segment_allocation__c,PSP_CORE_r20_Morsiky__c=moriskkyScore,PSP_CORE_r20_BMQ_Necessity__c=BMQ_Necssity,PSP_CORE_r20_BMQ_Concerns__c=BMQ_Concerns,PSP_CORE_r20_BIPQ__c=BIPQ,PSP_CORE_r20_Segment_allocation__c=segmentation.PSP_CORE_r20_Segment_allocation__c );
          System.debug(userSegmentation);
          segmentList.add(userSegmentation);
          
        }
        
         if(morisky8_Score!=null && morisky8_Score!=''){
           
           
          PSP_CORE_r20_Segementation__c segmentation=[select PSP_CORE_r20_Segment_allocation__c from PSP_CORE_r20_Segementation__c where PSP_CORE_r20_Morsiky__c=:morisky8_Score AND  PSP_CORE_r20_Master_Data__c=true AND (PSP_CORE_r20_Segment_allocation__c='Intensive' OR PSP_CORE_r20_Segment_allocation__c='Standard') limit 1];
      
          /*Case mCase=[select id,PSP_CORE_Adherence_Outcome__c,PSP_CORE_MAT_Completed_Date__c,PSP_CORE_Morisky_Score__c,PSP_CORE_r20_Completion_of_Assessments__c,PSP_CORE_r20_Default_Segmentation__c from Case where id=:sResponse.PSP_CORE_r20_Case__c];
          
          mCase.PSP_CORE_Adherence_Outcome__c=segmentation.PSP_CORE_r20_Segment_allocation__c;
          mCase.PSP_CORE_MAT_Completed_Date__c=Date.Today();
          mCase.PSP_CORE_Morisky_Score__c =sResponse.PSP_CORE_r20_Score_1__c;
          mCase.PSP_CORE_Validation_Check__c= false;
          update mCase;*/
           /*updated on 5/16/16 for morisky survey*/
          list<Task> taskObj = [Select Id,PSP_CORE_r20_Adhere_Outcom_From_Survey__c,PSP_CORE_r20_MAT_Complet_Date_Frm_Survey__c,PSP_CORE_r20_Morisky_Score_From_Survey__c from Task where WhatId =:sResponse.PSP_CORE_r20_Case__c and RecordType.DeveloperName ='PSP_CORE_Morisky_Assessment' and (Status != 'Completed' AND Status != 'Cancelled' AND Status != 'Not Assigned' )  and ActivityDate != Null ORDER BY ActivityDate DESC limit 1];
          if(!taskObj.isEmpty()){
          taskObj[0].PSP_CORE_r20_Adhere_Outcom_From_Survey__c=segmentation.PSP_CORE_r20_Segment_allocation__c;
          taskObj[0].PSP_CORE_r20_MAT_Complet_Date_Frm_Survey__c=Date.Today();
          taskObj[0].PSP_CORE_r20_Morisky_Score_From_Survey__c =sResponse.PSP_CORE_r20_Score_1__c;
          PSP_CORE_r20_Segementation__c userSegmentation=new PSP_CORE_r20_Segementation__c(PSP_CORE_r20_Case__c=sResponse.PSP_CORE_r20_Case__c,PSP_CORE_r20_Morsiky__c=morisky8_Score,PSP_CORE_r20_Segment_allocation__c=segmentation.PSP_CORE_r20_Segment_allocation__c);
          
          update taskObj ;
          segmentList.add(userSegmentation);
          
          System.debug(userSegmentation);
           }/*end of update on 5/16/16 for morisky survey*/

          
        }
    }
    
     if(!segmentList.isEmpty() && APS_Util_Constants.SEGREC){
         APS_Util_Constants.SEGREC=false;
         System.debug(segmentList);
         Database.insert(segmentList);
     }
     if(!updateSegmentList.isEmpty() && !responseToBeUpdated.isEmpty() && APS_Util_Constants.SEGREC2){
         APS_Util_Constants.SEGREC2=false;
         Database.update(responseToBeUpdated);
         Database.update(updateSegmentList);
     }
  }
}