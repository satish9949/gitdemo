trigger PSP_CORE_r20_CaptureResponse on PSP_CORE_r20_Segementation__c (after insert) {
   if(System.label.PSP_CORE_r20_Response_Trigger.equals('Active')){
    if(Trigger.isAfter && Trigger.isInsert){
        PSP_CORE_r20_CaptureResponse_helper.assignResponse(Trigger.new);
    }
   } 
}